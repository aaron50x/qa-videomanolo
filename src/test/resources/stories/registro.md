Registro de Usuario
====
**Como** usuario no logueado **quiero** registrarme en el sitio **para** poder a acceder a los beneficios de los usuarios logueados

## Criterios de Aceptación
### Visuales

#### Generales
  - Titulo "Registro de usuario" al 100% de la primera fila de la grilla
  - Debe visualizarse el **header completo** del sitio de un **usuario no logueado**
  - Deberá tener un input para ingresar el nombre con el label "Nombre" arriba del input
  - Deberá tener un input para ingresar el apellido con el label "Apellido" arriba
  - Deberá tener un input para ingresar el correo electronico con el label "Correo electronico" arriba
  - Deberá existir otro input para repetir el correo electronico con el label "Repite tu correo electronico" arriba
  - Deberá existir un input para ingresar el nombre de usuario con el label "Nombre de usuario" arriba
  - Deberá existir un input para ingresar la contraseña con su respectivo label arriba
  - Debera existir un input para repetir la contraseña con su respectivo label arriba
  - El box del formulario de registro está centrado en la pantalla
  - Existirá un botón de submit llamado Registrarme
  - Deberá existir un checkbox con el label "Acepto los terminos y condiciones"
  - El texto "terminos y condiciones" es un link
  
#### Escritorio
  - Existirá una fila con los campos Nombre y Apellido, en ese orden
  - Existirá una fila debajo de la fila de nombre y apellido con los campos Correo electronico y Repite tu correo electronico
  - Deberá existir una fila con el campo Nombre de usuario debajo de la fila de Correos
  - Deberá existir una fila con los campos contraseña y repite tu contraseña debajo del Nombre de usuario
  - Deberá existir una fila con el checkbox de Terminos y condiciones con el botón de Registrarme al lado debajo de los campos de contraseña
  
#### Movil
  - Todos los campos estarán, uno debajo del otro ocupando el 100% de la grilla
  - El orden de los campos en modo descendente es Nombre, Apellido, Correo electronico, Repite tu correo electronico, Nombre de usuario, Contraseña, Repite tu contraseña, Acepto terminos y condiciones, Submit Registrarme.
  
### Funcionales
  - Todos los campos son obligatorios
  - Los campos nombre, apellido, Correo electronico, Repite tu correo electronico y Nombre de usuario tienen las siguientes caracteristicas:
    - Tienen maximo de 30 caracteres
    - Tienen un minimo de 2 caracteres
    - Solo reciben caracteres alfanumericos
    - No aceptan espacios
  - Al dar click en el link de Terminos y condiciones, se abrirá una nueva ventana o tab.
  - Al dar click en el botón de Registrarme pueden ocurrir las siguientes opciones:
    - Si algún campo está errado, mostrará un mensaje de error
    - Si todo está OK me redireccionará a la Home con el **header completo** como **usuario logueado**