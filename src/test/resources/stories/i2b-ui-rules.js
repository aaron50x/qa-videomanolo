rule("%{object} placeholder is %{text}",
    function(objectName, parameters){
	    var obj = find(parameters.object);
	    var ph = obj.placeholder; 
	    if (ph !== parameters.text) {
	    	throw new Error("Placeholder value '" + ph + "' is not expected '" + parameters.text + "'");
	    }
	})