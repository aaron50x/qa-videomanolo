Home
====
**Como** cualquier usuario
**quiero** ver la página de inicio
**para** conocer las promociones y diferentes servicios del sitio

## Criterios de Aceptación
### Visuales

#### Generales
  - En todos los dispositivos la página debe cubrir el 100% del ancho y el alto del viewport
  - El nombre de la aplicación "Video Manolo" debe verse
  - Deben verse las últimas 5 películas agregadas al catálogo
  - Debe verse el link para iniciar sesión
  - Debe verse la caja de texto de búsqueda
  - Debe verse la lupa de la caja de texto de búsqueda 
  - La caja de texto de búsqueda debe tener la marca de agua "Título, director, año o género"

#### Escritorio
  - El título debe verse como primer elemento de la página
  - Deben verse las películas recientes en un carrusel de 2 peliculas a la vez
  - Debajo de las películas recientes debe verse la caja de búsqueda
  - En la parte superior derecha debe verse el link a inicio de sesion
  - En la parte superior derecha debe verse el link a registrarse
  - El link inicio de sesion debe estar a la izquierda de registrarse
  - Los links inicio de sesión y registrarse deben verse en blanco siempre
  - No deben aparecer nunca las barras de desplazamiento para la página

#### Móvil
  - El título debe estar en un header 100% de ancho fondo negro, letra blanca
  - Debajo del header debe estar la caja de búsqueda 100% ancho
  - La caja de búsqueda debe mostrar el ícono de la lupa alineado a la derecha
  - Debajo de la caja de búsqueda deben aparecer los links inicio de sesión y registro
  - Debajo de los links deben mostrarse las películas recientes
  - Cada película mostrada debe ocupar el ancho 100%
  - La sección de películas debe ocupar el resto del viewport
  - Deben existir las secciones header, busqueda, links y content
  - En caso de ser más de una película debe ver la barra de desplazamiento horizontal
  - Nunca debe aparecer barra de desplazamiento vertical
  - Se debe mostrar una película a la vez en un carrusel horizontal

### Funcionales

#### Links

  - Al hacer clic en el título "Video Manolo" debe enviarme a la página home, incluyendo desde la misma página home
  - Al hacer clic en el link inicio de sesión debe enviarme a la página login
  - Al hacer click en registrarse debe enviarme a la página de registro

#### Búsqueda

 - Al hacer clic en la lupa debe realizarse la búsqueda enviandome a la página de resultados
 - Al presionar enter debe realizarse la búsqueda enviandome a la página de resultados

##### Autocompletar

 - Los resultados de autocompletar deben ser 5 máximo
 - Al ingresar la primera letra debe activarse la funcionalidad
 - No debe discriminarse por mayúsculas o minúsculas, es decir, B será igual a b
 - La caja de autocompletar debe mostrar los resultados ordenados alfabéticamente y luego por año
 - La caja de autocompletar debe mostrar:
   - Título
   - Año
   - Género
   - Director
   - Y la imagen de muestra
   - Este debe ser el layout de la caja de autocompletar, cuando la palabra "Cabal" sea ingresada en la caja de búsqueda:
     <table style="margin: 0; padding:0; border-collapse: collapse; border: 1px black solid;">
       <tr>
         <td><img src="http://www.gstatic.com/tv/thumb/movieposters/173378/p173378_p_v7_aa.jpg" width="80"></td><td></td>
         <td style="padding-left: 1em; padding-right: 1em; border-left: 1px solid grey;">
         	<strong>El <span style="background-color: lightgreen;">Cabal</span>lero Oscuro (2008)</strong>
         	<div style="font-size: 75%;">Acción, Drama, Sci-fi</div>
         	<div style="font-size: 75%;">Dirigida por Christopher Nolan</div>
         </td>
       </tr>
     </table>
 - Todos los resultados deben mostrar resaltados los caracteres que hacen match en el título, género, año o director
 - Esta funcionalidad sólo debe mostrar máximo 5 resultados cada vez, y en caso de existir más de 5 resultados, debe mostrarse el link Ver todos los resultados
 - Al hacer clic en el link Ver más resultados debe enviarme a la lista de resultados de la búsqueda ingresada
 - Si se presiona la tecla enter en la caja de búsqueda debe enviarme a la lista de resultados de la búsqueda ingresada
 - Si se hace clic en el ícono lupa debe enviarme a la lista de resultados de la búsqueda ingresada
 
#### Películas

##### Escritorio

  - Las películas deben mostrarse sin ningún desplazamiento manual
  - El desplazamiento horizontal debe ser automático cada 5 segundos
  - Debe mostrarse solo la imagen de una película a la vez
  
##### Móvil
  
  - Se deben mostrar las 5 películas más recientes ordenadas por la fecha de registro
  - Siempre debe mostrarse sólo una película
  - En caso de existir más de una película permitir desplazarse horizontalmente a través de los resultados
  - Al hacer click en una película debe llevarme a la ficha de ésta
  - Las imágenes de las películas se deben cargar de forma diferida