Agregar al Carro
================
## Escenario
Estoy en la página de resultados de búsqueda
## Descripción
**Como** cualquier usuario
**quiero** agregar películas a mi carro de compras
**para** comprarlas posteriormente

## Criterios de Aceptación 
### Funcionales
  - Al hacer clic en el botón "+ al carro" debe mostrarme el mensaje "Película agregada al carro"
  - En el header junto al nombre de usuario o el link "Iniciar sesion" debe mostrarme el número de películas que tengo en el carro de compras