Login
====
**Como** usuario no logueado **quiero** loguearme en el sitio **para** poder acceder a las zonas privadas del sitio 

## Criterios de Aceptación
### Visuales

#### Generales
  - En todos los dispositivos la página de login debe cubrir el 100% del ancho y el alto del viewport
  - Debe visualizarse el **header completo** del sitio de un **usuario no logueado**
  - Debe visualizarse el titulo "Ingresa a tu cuenta"
  - Debe contener los siguientes elementos de formulario:
    - input para colocar el nombre de usuario con su label "Nombre de usuario" encima del input
    - input para colocar la contraseña con el label "Contraseña" encima del input
    - link a pagina de restablecer contraseña con el texto "¿Olvida tu contraseña? :P" 
    - boton para hacer submit del formulario con el texto "Entrar"
    - El link de restablacer contraseña estará al lado iquierdo del botón de submit del formulario
    - El input del nombre de usuario estará encima del label de la contraseña
    - El input de la contraseña estará encima del boton de submit y el link de recordar contraseña
  

#### Escritorio
  - El texto con invitación a hacer login deberá estar al lado izquierdo del box del formulario de login
  - El box del formulario de contacto ocupa el 50% de la grilla al lado derecho
  - El texto de invitación ocupa el 50% de la grilla al lado izquierdo
 

#### Móvil
  - El texto de invitación a hacer login ocupara 100% de la grilla
  - El texto de invitación estará encima del box del formulario de login
  - El formulario de login ocupará el 100% de la grilla
  
### Funcionales
#### Formulario de login

  - El input de nombre de usuario debe permitir solo caracteres alfanumericos y no más de 30 caracteres
  - El minimo de caracteres permitidos en el input de Nombre de usuario es de 2 caracteres
  - El input de contraseña debe permitir caracteres alfanumericos, maximo 8, minimo 6.
  - Ninguno de los input podrá recibir espacios en blanco
  - Si se escriben espacios en blanco al final o al inicio dentro de un input, no deberan tenerse en cuenta
  - Al dar click en el link Recordar contraseña, deberá dirigirme a la pagina /recordar.html
  - Todos los input son requeridos
  - Al dar click en el boton entrar podrán ocurrir las siguientes acciones:
    - Si los datos suministrados son correctos, una redirección al home del sitio, con **header completo** de **usuario logueado**
    - Si los datos ingresados no son correctos debe mostrarme un mensaje de error
  