#	[ ] Debe aparecer el título con el nombre de aplicación "Video Manolo"
#	[ ] Debe aparecer la caja de texto de usuario con la marca de agua "Nombre de Usuario"
#	[ ] El campo "Nombre de Usuario" debe tener el caracter "*" indicando que es requerido
#	[ ] Debe aparecer la caja de texto de contrasena con la marca de agua "Contrasena"
#	[ ] El campo "Contrasena" debe tener el caracter "*" indicando que es requerido
#	[ ] Debe aparecer el botón "Ingresar"
#	[ ] El orden de los campos debe ser "Nombre de Usuario", "Contrasena" y botón "Ingresar"

@script i2b-ui-rules.js

# Definimos los objetos
@objects
# elemento      locator    valor
  title         id         title
  username      id         txtUsername
  password      id         txtPassword
  usernameTag   id         tagUsername
  passwordTag   id         tagPassword
  loginButton   id         btnLogin

== Login Page ==
# Debe aparecer el título con el nombre de aplicación "Video Manolo"
  title:
    text is "Video Manolo"

# Debe aparecer la caja de texto de usuario vacia
  username:
    text is ""

# El campo "Nombre de Usuario" debe tener el caracter "*" indicando que es requerido
  usernameTag:
    text is "*"

# Debe aparecer la caja de texto de contrasena vacia
  password:
   text is ""

# El campo "Contrasena" debe tener el caracter "*" indicando que es requerido
  passwordTag:
    text is "*"
# Debe aparecer el botón "Ingresar"
  loginButton:
    visible
    text is "Ingresar"
 
# El orden de los campos debe ser "Nombre de Usuario", "Contrasena" y botón "Ingresar"
  username:
    above password
    right-of usernameTag
  password:
    above loginButton
    right-of passwordTag