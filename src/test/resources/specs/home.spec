# Lista de objetos
@objects
# Nombre            selector  valor 
  titulo              css     h1
  linklogin            id     loginsignuplink
  search               id     searchtextinput
  lupa                 id     searchsubmitbutton
  carrusel             id     moviesBox
  peliculas           css     #moviesBox div
  
# Seccion  
== Home ==
# El nombre de la aplicación "Video Manolo" debe verse  
  titulo:
    text is "Video Manolo"
	visible

# Debe verse el link "Iniciar Sesion"
  linklogin:
    text is "Iniciar Sesion"
    visible
    
# Debe verse la caja de texto de búsqueda
  search:
    visible

# Debe verse la lupa de la caja de texto de búsqueda
  lupa:
    visible

# Deben verse las últimas 5 películas agregadas al catálogo
  carrusel:
    visible
    count visible peliculas is 5
    