package com.i2btech.qa.videomanolo.home;

import static org.junit.Assert.*;

import org.junit.Test;

import com.i2btech.qa.videomanolo.BaseTest;
/**
 * Al hacer clic en el título "Video Manolo" debe enviarme a la página home, incluyendo desde la misma página home
 * Al hacer clic en el link inicio de sesión debe enviarme a la página login
 * Al hacer click en registrarse debe enviarme a la página de registro
 * @author admin
 *
 */
public class FunctionalLinksTestCase extends BaseTest {
	private String browser = "firefox";
	/**
	 * Al hacer clic en el título "Video Manolo" debe enviarme a la página home, incluyendo desde la misma página home
	 */
	@Test
	public void testTitle() {
		go("http://localhost:5555/busqueda.html"); // Preparar la prueba, o definir el escenario
		findOneByCSS("h1").click(); // Pasos de la prueba
		assertTrue(currentUrl().endsWith("home.html")); // validación
		
	}
	/**
	 * Al hacer clic en el link inicio de sesión debe enviarme a la página login
	 */
	@Test
	public void testLoginLink() {
		go("http://localhost:555/home.html");
		findById("loginsignuplink").click(); // Nombre del elemento de acuerdo al standard
		
		assertTrue(currentUrl().endsWith("login.html")); // Ok!
	}
	
	

}
