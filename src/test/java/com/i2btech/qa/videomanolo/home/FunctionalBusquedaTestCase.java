package com.i2btech.qa.videomanolo.home;

import static org.junit.Assert.*;

import org.junit.Test;

import com.i2btech.qa.videomanolo.BaseTest;
/**
 * Búsqueda
 *
 * Al hacer clic en la lupa debe realizarse la búsqueda enviandome a la página de resultados
 * Al presionar enter debe realizarse la búsqueda enviandome a la página de resultados
 *
 */
public class FunctionalBusquedaTestCase extends BaseTest{
	private String browser = "firefox";
	
	/**
	*Al hacer clic en la lupa debe realizarse la búsqueda enviandome a la página de resultados
	*/
	@Test
	public void testSubmitButton() {
		//Definiri escenario
		go("http://localhost:5555/busqueda.html"); 
		
		// Pasos de la prueba
		sendText("searchtextinput", "prueba");
		findById("searchsubmitbutton").click();
		
		//sendText("searchtextinput",  "Hola soy arito\n"   );
		
		// validación
		assertTrue(currentUrl().endsWith("resultadoBusqueda.html")); 
		
	}
	
	/**
	*Al presionar enter debe realizarse la búsqueda enviandome a la página de resultados
	*/
	@Test
	public void testSearchEnter() {
		//Definiri escenario
		go("http://localhost:5555/busqueda.html"); 
		
		// Pasos de la prueba
		sendText("searchtextinput", "Hola soy Arito\n");
		
		// validación
		assertTrue(currentUrl().endsWith("resultadoBusqueda.html")); 
		
	}

}
