package com.i2btech.qa.videomanolo.home;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.i2btech.qa.videomanolo.BaseTest;

public class FunctionalAutocompleteTest extends BaseTest {
	/**
	 * La caja de autocompletar debe mostrar los resultados ordenados alfabéticamente y luego por año
	 * Datos de Ejemplo:
	 *   Peliculas:
	 *     Titulo                  Año           Director
	 *     Avengers                2010          Roger Villamarin
	 *     Armagedeon              1999          Zaida Salamanca
	 *     Alvin y Las Ardillas    2000          Danito
	 *     Arman "El Salvaje"      2015          Arman Sampoya
	 *     Alieniginas             2004          Eliana "Elí" Velasquez
	 *     Base Naval              2010          Rihanna
	 */
	@Test
	public void testSort() {
		// Definir escenario
		go("http://localhost:5555/home.html");
		
		// Ejecutar los pasos
		sendText("searchtextinput", "a");
		List<WebElement> movies = findByCSS("#autocompleteBox li");
		
		// Las validaciones 
		assertEquals("Alieniginas", movies.get(0).getText());
		assertEquals("Alvin y Las Ardillas", movies.get(1).getText());
		assertEquals("Armagedeon", movies.get(2).getText());
		assertEquals("Arman \"El Salvaje\"", movies.get(3).getText());
		assertEquals("Avengers", movies.get(4).getText());
		
	}
	
	/**
	 * 
	 */
	@Test
	public void testSortB() {
		// Definir escenario
		go("http://localhost:5555/home.html");
		
		// Ejecutar los pasos
		sendText("searchtextinput", "b");
		List<WebElement> movies = findByCSS("#autocompleteBox li");
		
		// Las validaciones 
		assertEquals("Base Naval", movies.get(0).getText());
		
	}

	@Test
	public void testSortC() {
		// Definir escenario
		go("http://localhost:5555/home.html");
		
		// Ejecutar los pasos
		sendText("searchtextinput", "c");
		List<WebElement> movies = findByCSS("#autocompleteBox li");
		
		// Las validaciones 
		assertTrue(movies.isEmpty());
		
	}


}
