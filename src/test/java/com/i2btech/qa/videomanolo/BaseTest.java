package com.i2btech.qa.videomanolo;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BaseTest {
	protected static WebDriver wd;
	@BeforeClass
	public static void startup() throws MalformedURLException {
		//java.net.URL url = new URL("http://192.168.1.180:4444/wd/hub");
		//wd = new RemoteWebDriver(url, DesiredCapabilities.firefox());
		wd = new FirefoxDriver();
	}
	
	public void go(String url) {
		wd.get(url);
	}
	
	public void sendText(String id, String value) {
		findById(id).sendKeys(value);
	}
	
	public WebElement findById(String id) {
		return wd.findElement(By.id(id));
	}
	
	public WebElement findOneByCSS(String cssLocator) {
		return wd.findElement(By.cssSelector(cssLocator));
	}
	
	public List<WebElement> findByCSS(String cssLocator) {
		return wd.findElements(By.cssSelector(cssLocator));
	}
	
	public String currentUrl() {
		return wd.getCurrentUrl();
	}
	
	@AfterClass
	public static void shutdown() {
		wd.close();
		wd.quit();
	}


}
