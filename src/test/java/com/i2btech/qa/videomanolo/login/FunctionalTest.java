package com.i2btech.qa.videomanolo.login;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Inicio de Sesión
 * 
 * COMO usuario anónimo 
 * QUIERO iniciar sesión 
 * PARA utilizar las funcionalidades privadas
     del sistema, como alquilar una pelicula
 *
 * [ ] Debe permitir un máximo de 20 caracteres en el campo "Nombre de Usuario"
 * [ ] Debe permitir un máximo de 10 caracteres en el campo "Contraseña"
 * [ ] El campo "Nombre de Usuario" debe ser requerido
 * [ ] El campo "Password" debe ser requerido 
 * [ ] Al ingresar un usuario/contraseña inválido debo ver el mensaje "Usuario/contraseña inválido, intente nuevamente"
 * [ ] Al ingresar un usuario/contraseña correcto, debo ver la página de búsqueda con el nombre de usuario en la parte superio
 * @author admin
 */
public class FunctionalTest {
	private static WebDriver wd;
	@BeforeClass
	public static void startup() throws MalformedURLException {
		java.net.URL url = new URL("http://192.168.1.180:4444/wd/hub");
		wd = new RemoteWebDriver(url, DesiredCapabilities.safari());
	}
	
	@AfterClass
	public static void shutdown() {
		wd.close();
		wd.quit();
	}
	
	/**
	 * Debe permitir un máximo de 20 caracteres en el campo "Nombre de Usuario"
	 */
	@Test
	public void testUsernameMaxChars() {
		wd.get("http://192.168.1.180:5555/login.html");
		WebElement uname = wd.findElement(By.id("txtUsername")); // Aquí usamos la convención acordada
		uname.sendKeys("123456789012345678901234567890");
		
		assertEquals(uname.getAttribute("value"), "12345678901234567890");
	}
	
	/**
	 * Debe permitir un máximo de 10 caracteres en el campo "Contraseña"
	 */
	@Test
	public void testPasswordMaxChars() {
		wd.get("http://192.168.1.180:5555/login.html");
		WebElement pass = wd.findElement(By.id("txtPassword")); // Aquí nuevamente usamos la convención acordada
		pass.sendKeys("xxxxxxxx10xxxxxxxx10xxxxxxxx10xxxxxxxx10");
		assertEquals(pass.getAttribute("value"), "xxxxxxxx10");
	}
	
	/**
	 * El campo "Nombre de Usuario" debe ser requerido
	 */
	@Test
	public void testUsernameIsRequired() {
		wd.get("http://192.168.1.180:5555/login.html");
		WebElement btnLogin = wd.findElement(By.id("btnLogin")); // nuevamente la convención acordada
		btnLogin.click();
		WebElement msg = wd.findElement(By.id("msgError")); // de nuevo convención acordada
		assertTrue(msg.getText().contains("Nombre de Usuario requerido"));
	}
	
	/**
	 * El campo "Password" debe ser requerido
	 */
	@Test
	public void testPasswordIsRequired() {
		wd.get("http://192.168.1.180:5555/login.html");
		WebElement btnLogin = wd.findElement(By.id("btnLogin")); // nuevamente la convención acordada
		btnLogin.click();
		WebElement msg = wd.findElement(By.id("msgError")); // de nuevo convención acordada
		assertTrue(msg.getText().contains("Contrasena requerido"));
	}
	

	/**
     * Al ingresar un usuario/contraseña inválido debo ver 
     * el mensaje "Usuario/contraseña inválido, intente nuevamente"
	 */
	@Test
	public void testInvalidUser() {
		wd.get("http://192.168.1.180:5555/login.html");
		WebElement user = wd.findElement(By.id("txtUsername")); // convención acordada
		WebElement pass = wd.findElement(By.id("txtPassword")); // convención acordada
		WebElement btnLogin = wd.findElement(By.id("btnLogin")); // convención acordada
		
		user.sendKeys("asantiago");
		pass.sendKeys("testingSelenium");
		btnLogin.click();
		
		WebElement msg = wd.findElement(By.id("msgError")); // convención acordada
		assertTrue(msg.getText().contains("Usuario/contrasena invalido, intente nuevamente"));
		
	}
	
	/**
	 * Al ingresar un usuario/contraseña correcto, 
	 * debo ver la página de búsqueda con el nombre 
	 * de usuario en la parte superior
	 */
	@Test
	public void testValidUser() {
		wd.get("http://192.168.1.180:5555/login.html");
		WebElement user = wd.findElement(By.id("txtUsername")); // convención acordada
		WebElement pass = wd.findElement(By.id("txtPassword")); // convención acordada
		WebElement btnLogin = wd.findElement(By.id("btnLogin")); // convención acordada
		
		user.sendKeys("asantiago");
		pass.sendKeys("Qpalwosk10");
		btnLogin.click();
		
		WebElement loginBox = wd.findElement(By.id("msgLogin"));
		
		assertTrue(wd.getCurrentUrl().endsWith("busqueda.html"));
		assertTrue(loginBox.getText().contains("Bienvenido Aaron Santiago"));
		
	}
}
